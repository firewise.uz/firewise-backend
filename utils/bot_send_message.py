import os

import requests
from environs import Env

from core.settings import BASE_DIR

ENV_URL = os.path.join(BASE_DIR, ".env")
env = Env()
env.read_env(ENV_URL)


class BotSendToUser:
    def __init__(self, token, chat_id):
        self.token = token
        self.chat_id = chat_id

    def send_message(self, text):
        url = f"https://api.telegram.org/bot{self.token}/sendMessage"
        params = {
            "chat_id": self.chat_id,
            "text": text,
        }

        try:
            response = requests.post(url, params=params)
            response.raise_for_status()  # Если статус ответа не 200, вызывается исключение HTTPError
        except requests.exceptions.RequestException as e:
            print(f"Ошибка при отправке сообщения: {e}")
            return False
        else:
            if response.status_code == 200:
                return True

    def send_document(self, file_path):
        url = f"https://api.telegram.org/bot{self.token}/sendDocument"
        f = open(file_path, "rb")
        file_byte = f.read()
        f.close()
        params = {
            "chat_id": self.chat_id,
        }
        response = requests.post(
            url, params=params, files={"document": (f.name, file_byte)}
        )
        if response.status_code == 200:
            return (True,)
        return False, response.json()


bot = BotSendToUser(env.str("BOT_TOKEN"), env.str("CHAT_ID"))
