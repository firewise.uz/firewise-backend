FROM python:3.11

SHELL ["/bin/bash", "-c"]

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt update && apt install make && apt install vim nano -y
RUN pip install --upgrade pip

WORKDIR /app

RUN mkdir /app/static && mkdir /app/media

COPY requirements.txt /app/

RUN pip install -r requirements.txt

COPY . .
