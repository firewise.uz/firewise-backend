from rest_framework import serializers

from src.models import Region, District, Address


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ["name"]


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ["name"]


class AddressSerializer(serializers.ModelSerializer):
    district = DistrictSerializer(read_only=True)
    region = RegionSerializer(source="district.region", read_only=True)

    class Meta:
        model = Address
        fields = [
            "id",
            "address1",
            "address2",
            "lat",
            "lng",
            "postal_code",
            "metro",
            "district",
            "region",
        ]
