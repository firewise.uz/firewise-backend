from utils.regex import TRANSLATION_MAP
from rest_framework import serializers

from src.models import Document, Form, Company, Category, ListOfObjectRegion, ListOfObject
from src.serializers.address import AddressSerializer
from utils.regex import phone_regex


class ListOfObjectRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListOfObjectRegion
        fields = '__all__'


class ListOfObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListOfObject
        fields = (
            "id",
            "name",
            "image",
            "square",
            "places",
            "competitions",
            "fire_panels",
            "detectors",
            "total_number_of_equipment",
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        # Efficient null field removal and translation using dictionary comprehension
        representation = {
            TRANSLATION_MAP.get(key, key): (value if key != "square" else f"{value} кв.м.")
            for key, value in representation.items()
            if value is not None
        }

        return representation


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "title", "description", "image"]


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ["id", "title", "file", "type"]


class FormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields = ["id", "full_name", "organization", "phone_number", "email", "desc"]


class CompanySerializer(serializers.ModelSerializer):
    address = AddressSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = [
            "id",
            "name",
            "description",
            "phone1",
            "phone2",
            "email",
            "web_site",
            "address",
        ]


class SendVerificationCodeSerializer(serializers.Serializer):
    phone_number = serializers.CharField(validators=[phone_regex])
