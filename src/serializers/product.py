from rest_framework import serializers

from src.models import (
    Category,
    ProductImage,
    Product,
    AttributeValue,
    AttributeCategory,
    TypeCategory
)


class TypeCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeCategory
        fields = ["id", "title"]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "title", "description", "image"]


class AttributeValueSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = AttributeValue
        fields = ["title", "value", "image"]

    def get_image(self, obj):
        request = self.context.get('request')
        if obj.image and hasattr(obj.image, 'url'):
            return request.build_absolute_uri(obj.image.url)
        return None


class AttributeNameSerializer(serializers.ModelSerializer):
    attribute_values = AttributeValueSerializer(many=True, read_only=True)

    class Meta:
        model = AttributeCategory
        fields = ["name", "attribute_values"]


class ProductImageSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = ProductImage
        fields = ["image"]

    def get_image(self, obj):
        request = self.context.get('request')
        if obj.image and hasattr(obj.image, 'url'):
            return request.build_absolute_uri(obj.image.url)
        return None


class ProductSerializer(serializers.ModelSerializer):
    product_images = ProductImageSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ["id", "title", "description", "category", "product_images"]


class ProductDetailSerializer(serializers.ModelSerializer):
    product_images = ProductImageSerializer(many=True, read_only=True)
    attribute_names = AttributeNameSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = [
            "title",
            "description",
            "category",
            "product_images",
            "attribute_names",
        ]
