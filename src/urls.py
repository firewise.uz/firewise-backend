from django.urls import path

from src.views import (
    CategoryView,
    FormCreateView,
    CompanyRetrieveView,
    DocumentCustomFilterView,
    SendVerificationCodeView,
    ProductView,
    TypeCategoryListAPIView,
    ListOfObjectSerializerListAPIView,
    ListOfObjectRegionListAPIView
)

urlpatterns = [
    path("listofobjectregion/", ListOfObjectRegionListAPIView.as_view(), name="list_of_object_region_list"),
    path("listofobject/", ListOfObjectSerializerListAPIView.as_view(), name="list_of_object"),
    path("category/", CategoryView.as_view(), name="category"),
    path("typecategory/", TypeCategoryListAPIView.as_view(), name="type_category"),
    path("product/", ProductView.as_view(), name="product_list"),
    path("document/", DocumentCustomFilterView.as_view(), name="document-list"),
    path("form/", FormCreateView.as_view(), name="form-create"),
    path("company/", CompanyRetrieveView.as_view(), name="company-detail"),
]
