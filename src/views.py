from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response

from src.models import Document, Form, Company, Category, Product, TypeCategory, ListOfObjectRegion, ListOfObject
from src.serializers.company import (
    DocumentSerializer,
    FormSerializer,
    CompanySerializer,
    SendVerificationCodeSerializer,
    ListOfObjectRegionSerializer,
    ListOfObjectSerializer

)
from src.serializers.product import (
    CategorySerializer,
    ProductSerializer,
    ProductDetailSerializer,
    TypeCategorySerializer,
)
from utils import bot


class ListOfObjectRegionListAPIView(generics.ListAPIView):
    serializer_class = ListOfObjectRegionSerializer
    queryset = ListOfObjectRegion.objects.all()


class ListOfObjectSerializerListAPIView(generics.ListAPIView):
    serializer_class = ListOfObjectSerializer
    queryset = ListOfObject.objects.all()  # Define your default queryset here

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "region_type_id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER
            ),
        ]
    )
    def get(self, request, *args, **kwargs):
        region_type_id = request.query_params.get("region_type_id")

        # Ensure region_type_id is an integer (if applicable to your model)
        try:
            if region_type_id:
                region_type_id = int(region_type_id)
        except ValueError:
            return Response(
                {"detail": "Invalid region_type_id (must be an integer)."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        queryset = self.get_queryset().filter(
            region_type_id=region_type_id) if region_type_id else self.get_queryset()

        if not queryset.exists():
            return Response(
                [], status=status.HTTP_404_NOT_FOUND
            )

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)



class TypeCategoryListAPIView(generics.ListAPIView):
    serializer_class = TypeCategorySerializer
    queryset = TypeCategory.objects.all()


class CategoryView(generics.ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all().order_by("id")

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "type_category_id", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
            ),
        ]
    )
    def get_queryset(self):
        queryset = super().get_queryset()
        type_id = self.request.query_params.get("type_category_id")

        if type_id is not None:
            queryset = queryset.filter(type_id=type_id)

        return queryset


class DocumentCustomFilterView(generics.ListAPIView):
    serializer_class = DocumentSerializer

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "document_type", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
            ),
        ]
    )
    def get(self, request, *args, **kwargs):
        document_type = self.request.query_params.get("document_type", None)

        if document_type:
            queryset = Document.objects.filter(type=document_type)
            if not queryset.exists():
                return Response(
                    {"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND
                )
        else:
            return Response(
                {
                    "types": [
                        Document.DocumentTypes.DOCUMENT,
                        Document.DocumentTypes.CERTIFICATE,
                        Document.DocumentTypes.PROJECT,
                    ]
                }
            )

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class FormCreateView(generics.CreateAPIView):
    queryset = Form.objects.all()
    serializer_class = FormSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Bot send message
        bot.send_message(
            f"Full name: {serializer.validated_data.get('full_name')}\n"
            f"Organization: {serializer.validated_data.get('organization')}\n"
            f"Phone number: {serializer.validated_data.get('phone_number')}\n"
            f"Email: {serializer.validated_data.get('email')}\n"
            f"Description: {serializer.validated_data.get('desc')}"
        )
        return super().post(request, *args, **kwargs)


class CompanyRetrieveView(generics.RetrieveAPIView):
    serializer_class = CompanySerializer

    def get_object(self):
        return Company.objects.first()


class SendVerificationCodeView(generics.CreateAPIView):
    serializer_class = SendVerificationCodeSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone_number = serializer.validated_data.get("phone_number")

        return Response(
            data={"phone_number": phone_number}, status=status.HTTP_201_CREATED
        )


class ProductView(generics.ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        category_id = self.request.query_params.get("category_id", None)
        product_last_count = self.request.query_params.get("product_last_count", None)

        if category_id:
            return Product.objects.filter(category_id=category_id)

        elif product_last_count:
            product_last_count = int(product_last_count)
            return Product.objects.order_by("-created_at")[:product_last_count]

        else:
            return Product.objects.all()

    def retrieve(self, request, *args, **kwargs):
        product_id = request.query_params.get("product_id", None)
        if product_id:
            try:
                queryset = Product.objects.get(id=product_id)
                serializer = ProductDetailSerializer(queryset, context={'request': request})
                return Response(data=serializer.data)
            except Product.DoesNotExist:
                return Response(
                    {"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND
                )
        else:
            return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "product_id", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
            ),
            openapi.Parameter(
                "category_id", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
            ),
            openapi.Parameter(
                "product_last_count", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
            ),
        ]
    )
    def get(self, request, *args, **kwargs):
        if 'product_id' in request.query_params:
            return self.retrieve(request, *args, **kwargs)
        else:
            return super().get(request, *args, **kwargs)
