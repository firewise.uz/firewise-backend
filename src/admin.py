from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from src.models import (
    TypeCategory,
    Category,
    Product,
    ProductImage,
    AttributeCategory,
    AttributeValue,
    Region,
    District,
    Address,
    Document,
    Form,
    Company,
    ListOfObjectRegion,
    ListOfObject
)


@admin.register(ListOfObjectRegion)
class ListOfObjectRegionModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name")
    ordering = ("id", "name")


@admin.register(ListOfObject)
class ListOfObjectModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name", "region_type")
    ordering = ("id", "name", "region_type")


@admin.register(TypeCategory)
class DistrictModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "title", "description")
    ordering = ("id", "title")


@admin.register(Category)
class CategoryModelAdmin(ImportExportModelAdmin):
    list_display = ("title", "description", "image", "type")
    list_filter = ("type",)
    ordering = ("id", "type")


@admin.register(Document)
class DocumentModelAdmin(ImportExportModelAdmin):
    list_display = ("title", "file", "type")
    search_fields = ("title", "type")
    list_filter = ("type",)
    ordering = ("id",)


@admin.register(Form)
class FormModelAdmin(ImportExportModelAdmin):
    list_display = ("full_name", "organization", "phone_number", "email", "desc")
    search_fields = ("full_name", "organization", "phone_number", "email")
    ordering = ("id",)


class DistrictInline(admin.TabularInline):
    model = District
    extra = 1


class AddressInline(admin.TabularInline):
    model = Address
    extra = 1


@admin.register(Address)
class AddressModelAdmin(ImportExportModelAdmin):
    list_display = ("address1", "district", "lat", "lng")
    search_fields = ("address1", "district__name")
    list_filter = ("district",)
    ordering = ("id",)


@admin.register(Company)
class CompanyModelAdmin(ImportExportModelAdmin):
    list_display = ("name", "email")
    search_fields = ("name", "email")
    ordering = ("id",)


class AttributeValueInline(admin.TabularInline):
    model = AttributeValue
    extra = 1


class AttributeCategoryInline(admin.TabularInline):
    model = AttributeCategory
    # inlines = [AttributeValueInline]  # Nested inline for AttributeValue
    extra = 1


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 1


@admin.register(Product)
class ProductModelAdmin(ImportExportModelAdmin):
    inlines = [ProductImageInline]
    list_display = ("title", "category")
    search_fields = ("title",)
    list_filter = ("category",)
    ordering = ("id",)


@admin.register(AttributeCategory)
class AttributeCategoryModelAdmin(ImportExportModelAdmin):
    inlines = [AttributeValueInline]
    list_display = ("name", "product")
    ordering = ("id",)


@admin.register(Region)
class RegionModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name")
    filter = ("name",)
    ordering = ("id",)


@admin.register(District)
class DistrictModelAdmin(ImportExportModelAdmin):
    list_display = ("id", "name", "region")
    ordering = ("id", "name")
