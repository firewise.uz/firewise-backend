from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    phone1 = models.CharField(max_length=20)
    phone2 = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField()
    web_site = models.CharField(max_length=255, null=True, blank=True)
    address = models.ManyToManyField("Address", blank=True, related_name="companies")

    def __str__(self):
        return self.name


class Document(models.Model):
    class DocumentTypes(models.TextChoices):
        DOCUMENT = "document"
        CERTIFICATE = "certificate"
        PROJECT = "project"

    title = models.CharField(max_length=255, null=True, blank=True)
    file = models.FileField(upload_to="files/")
    type = models.CharField(max_length=30, choices=DocumentTypes.choices)

    def __str__(self):
        return self.title


class Form(models.Model):
    full_name = models.CharField(max_length=255)
    organization = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=20)
    email = models.EmailField()
    desc = models.TextField()

    def __str__(self):
        return self.full_name


class ListOfObjectRegion(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class ListOfObject(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to="images/")
    square = models.IntegerField(blank=True, null=True)
    places = models.IntegerField(blank=True, null=True)
    competitions = models.CharField(max_length=255, blank=True, null=True)
    fire_panels = models.CharField(max_length=255, blank=True, null=True)
    detectors = models.IntegerField(blank=True, null=True)
    total_number_of_equipment = models.IntegerField(blank=True, null=True)
    region_type = models.ForeignKey(
        "ListOfObjectRegion", on_delete=models.CASCADE, related_name="region_type"
    )

    def __str__(self):
        return self.name
