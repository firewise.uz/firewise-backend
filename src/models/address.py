from django.db import models


class Region(models.Model):
    name = models.CharField(max_length=55)

    def __str__(self):
        return self.name


class District(models.Model):
    name = models.CharField(max_length=55)
    region = models.ForeignKey(
        "Region", on_delete=models.CASCADE, related_name="districts"
    )

    def __str__(self):
        return self.name


class Address(models.Model):
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, null=True, blank=True)
    lat = models.CharField(max_length=255)
    lng = models.CharField(max_length=255)
    postal_code = models.IntegerField()
    metro = models.CharField(max_length=255, null=True, blank=True)
    district = models.ForeignKey(
        "District", on_delete=models.SET_NULL, null=True, related_name="address"
    )

    def __str__(self):
        return self.address1
