from django.db import models


class TypeCategory(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class Category(models.Model):
    type = models.ForeignKey(
        "TypeCategory", on_delete=models.CASCADE, related_name="categoryes"
    )
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to="images/", null=True, blank=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(
        "Category", on_delete=models.CASCADE, related_name="products"
    )

    @property
    def product_images(self):
        if self.product_images.all():
            return self.product_images.first().image.url
        return ""

    def __str__(self):
        return self.title


class ProductImage(models.Model):
    image = models.ImageField(upload_to="images/")
    product = models.ForeignKey(
        "Product", on_delete=models.CASCADE, related_name="product_images"
    )

    def __str__(self):
        return f"{self.product} - {self.image}"


class AttributeCategory(models.Model):
    name = models.CharField(max_length=255)
    product = models.ForeignKey(
        "Product", on_delete=models.CASCADE, related_name="attribute_names"
    )

    def __str__(self):
        return f"{self.product} - {self.name}"


class AttributeValue(models.Model):
    title = models.CharField(max_length=300, null=True)
    value = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to="images/", null=True, blank=True)
    attribute_category = models.ForeignKey(
        "AttributeCategory",
        on_delete=models.CASCADE,
        related_name="attribute_values",
    )

    def __str__(self):
        return f"{self.attribute_category.name} - {self.title}"
